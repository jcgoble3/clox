# Chapter 16: Scanning on Demand

- [Link to book chapter](https://craftinginterpreters.com/scanning-on-demand.html)
- [Previous chapter](15-a-virtual-machine.md)
- [Table of Contents](index.md)
- [Next chapter](17-compiling-expressions.md)

## 16.1: Spinning Up the Interpreter

- From the lede: clox has three distinct phases:
  - *scanner*, which produces tokens
  - *compiler*, which consumes tokens and produces bytecode
  - *virtual machine*, which executes bytecode
- C is annoying
  - Manual memory management is difficult
    - Java completely hides memory management
    - In C++, memory management can be written such that the compiler can
      verify its correctness
    - C forces us to do it all by hand and end up with strange errors or
      unexpected behavior when we get it wrong
  - Most errors are silent unless you explicitly check return values
    - Did you know that `printf()` can fail? It signals this with a
      negative return value.
    - Did you even know that `printf()` returned `int`, not `void`? I didn't.

## 16.2: A Token at a Time

- jlox eagerly scanned the whole script at once and just returned a list of tokens
- However, that would need a lot of memory management and code to go with it for clox
- Instead, clox will interleave the scanner and compiler such that a token
  is not scanned until the compiler asks for it
- This is possible because the grammar only requires one token of lookahead,
  so the compiler only needs at most two tokens at once
- I learned that string literals in C are eternal; their memory is not released
  until the program exits. Hence you can do things like `errorToken()` does.

## 16.3: A Lexical Grammar for Lox

- This chapter really has no theory to note, just a lot of coding and implementation.

## 16.4: Identifiers and Keywords

- clox uses a trie to identify reserved words
  - The name "trie" comes from the middle of the word "retrieval", which implies
    that it ought to be pronounced "tree"... but a tree is already a fundamental
    data structure, and a trie IS a tree. So to avoid confusion, we pronounce it
    "try" these days, which is probably confusing in itself, but less so than "tree".
  - Tries are actually a *deterministic finite automaton* (DFA), aka a *finite state machine*
