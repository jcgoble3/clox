# Chapter 18: Types of Values

- [Link to book chapter](https://craftinginterpreters.com/types-of-values.html)
- [Previous chapter](17-compiling-expressions.md)
- [Table of Contents](index.md)
- [Next chapter](19-strings.md)

## 18.1: Tagged Unions

- Dynamically typed languages need a way to determine the type of a value at runtime
- Both type and value need to be stored
- The classic solution is a *tagged union*: a struct with a field for the type and
  a union field for the value payload

## 18.2: Lox Values and C Values

- We just broke all of our code that depended on `Value`s being `double`s. :-(
- We need to be able to convert from our representation to a C value, and vice versa

## 18.3: Dynamically Typed Numbers

- Dynamic typing requires runtime errors when the user does something like `print -false`
- I learned that you can pass macros as parameters to macros!
- Code compiles and runs again

## 18.4: Two New Types

- For speed, it makes sense to have dedicated operations for certain values like
  `nil`, `true`, and `false`
- Lox treats `nil` and `false` as falsey and everything else as truthy
  - This is the same as Lua and Ruby
- We don't strictly need six opcode for the six comparison and equality operators,
  since three of them are inverses of the other three
- Equality has to check the type first, then the value
- Direct quote from the book: "PHP was designed by a Lovecraftian Eldritch god
  to destroy the mind." That is a fact.
