# Chapter 21: Global Variables

- [Link to book chapter](https://craftinginterpreters.com/global-variables.html)
- [Previous chapter](20-hash-tables.md)
- [Table of Contents](index.md)
- [Next chapter](22-local-variables.md)

## Lede

- If variables are slow, everything is slow
- Allocating a new hash table for every scope would be slow
- Hence, local variables in Lox will use a different strategy in the next chapter

## 21.1: Statements

- Variables in Lox require a declaration statement, meaning that our compiler
  needs to begin handling statements
- Declarations are a special kind of statement that aren't allowed in all
  places where statements are allowed
- Therefore our grammar places declarations at a different level than other
  statements
- This chapter will introduce expression statements, print statements, and
  variable declarations
- Lox tries to report as many compile-time errors as possible, with as few
  extraneous errors as possible, using *synchronization*.
  - When an error occurs, the parser goes into "panic mode". In panic mode,
    no errors are reported, until the parser is able to synchronize at a
    statement boundary (either a semicolon or a keyword that always starts
    a statement).

## 21.2: Variable Declarations

- To get variables working, we need three things:
  - Declaring a new variable (using `var`)
  - Getting the value of a variable
  - Storing a new value in an existing variable
- A variable declared without a value receives a default value of `nil`
- Global variables are stored in a hash table

## 21.3: Reading Variables

- Lox throws a runtime error if you try to access a variable that isn't
  declared; we check for that by checking whether it's in the hash table

## 21.4: Assignment

- The parser can't tell a variable access apart from a variable assignment
  until it sees the `=` token. But in a long statement like
  `menu.brunch(sunday).beverage = "mimosa";` this is a problem in a
  single-pass compiler because bytecode for the whole LHS is emitted before
  encountering the `=`.
- However, *most* of the LHS is still a normal variable access; it's only
  the last part (`.beverage`) immediately before the `=` that is a
  set operation instead of a get.
- At this stage, we only have bare variables to worry about
