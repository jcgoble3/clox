# Chapter 25: Closures

- [Link to book chapter](https://craftinginterpreters.com/closures.html)
- [Previous chapter](24-calls-and-functions.md)
- [Table of Contents](index.md)
- [Next chapter](26-garbage-collection.md)

## Lede

- Closures require tracking the lexical scope of every surrounding function
- Closures also break the stack semantics of local variables for the variables
  that are closed over, but most local variables are not closed over
- We don't want to slow down all locals just for a small few that are used
  in closures
- clox's solution is modeled after Lua

## 25.1: Closure Objects

- The same function declaration can result in multiple unique closures if
  its declaration is executed with different values for the local variables
  it closes over.
- The compiler produces a single function object, but at runtime that can
  become multiple different closures, so when the declaration is executed
  at runtime, we wrap the function in a closure object

## 25.2 Upvalues

- Closed-over variables can outlive the function where they were declared,
  so aren't guaranteed to still be on the stack
- clox can't automatically allocate a local variable that will be closed over
  on the heap because its single-pass compiler can't know about the closure until
  after the declaration bytecode has been emitted
- The solution needs to keep a closed-over varaible on the stack until it is
  actually closed over at runtime
- We borrow from Lua the concept of an **upvalue** -- a local variable in an
  enclosing function, of which each closure keeps an array
  - Upvalues initially point into the stack, but when the variable leaves the stack,
  the value is moved to a place where the upvalue can still find it
  - We know at compile-time the number of upvalues in a closure, which variables
  they refer to, and where they initially are on the stack
- A variable declared in one function could be closed over by a deeply nested
  function, meaning that the variable could be off the stack before the declaration
  that closes over it is executed
  - The solution is to allow capturing either a local variable or an upvalue in the
    immediately enclosing function
- clox's OP_CLOSURE opcode is variable length, specifying the upvalues inline in
  the bytecode operands. I would prefer to store this data on the function object
  as Lua does to have a fixed-length instruction.

## 25.3 Upvalue Objects

- No real theory here, just straight implementation

## 25.4 Closed Upvalues

- Closures capture variables, not values. This means that if you have two closures
  over the same variable, and one of those closures mutates the variable, the other
  closure will read the new value, not the value that was present at the initial
  time of closure
- An **open** upvalue is one that points into the Lox stack
- A **closed** upvalue is one that points into the heap
- An upvalue still in scope must remain on the stack since other code may expect
  to find it there, therefore an upvalue should be closed as late as possible, which
  is when it goes out of scope as a local variable
- This sharing requires two sibling closures that capture the same variable to share
  the upvalue object
