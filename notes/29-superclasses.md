# Chapter 29: Superclasses

- [Link to book chapter](https://craftinginterpreters.com/superclasses.html)
- [Previous chapter](28-methods-and-initializers.md)
- [Table of Contents](index.md)
- [Next chapter](30-optimization.md)

## 29.1: Inheriting Methods

- clox implements inheritance similar to method declarations, by emitting
  bytecode to mutate the class to add the effect of inheritance
- clox's approach to inheritance is the so-called "copy-down" method: all
  methods are copied directly from the superclass into the subclass so that
  only one hash table lookup is needed to find them at runtime
  - This works in Lox because Lox classes are *closed*: the class's methods
    cannot be altered at runtime; i.e. they cannot be "monkey-patched"
  - I do not like this and would prefer walking a list of superclasses to
    enable runtime modification of a class; monkey-patching is generally
    bad but can be useful occasionally

## 29.2: Storing Superclasses

- The keyword `super` (referencing the superclass) is implemented as an upvalue
  of the subclass's methods

## 29.3: Super Calls

- Access to `super` is done just like a regular dot lookup
- We also create a superinstruction for invoking a superclass method directly

## 29.4: A Complete Virtual Machine

- The implementation is now complete! Only a little optimization remains.
