#include "common.h"

const BagelValue BAGEL_NIL = NIL_VAL;
const BagelValue BAGEL_TRUE = BOOL_VAL(true);
const BagelValue BAGEL_FALSE = BOOL_VAL(false);

// Convert const char* to ObjString*
#define cts(chars) copyString((chars), (int)strlen(chars))

#define currentFrame (vm.frames[vm.frameCount - 1])

BagelValue bagel_error(const char* format, ...) {
    va_list args;
    va_start(args, format);
    runtimeError(format, args);
    va_end(args);
    return ERROR_VAL;
}

BagelValue bagel_newclassx(const BagelMethod* methods, const char* name,
                           BagelValue superclass) {
    if (!(IS_NIL(superclass) || IS_CLASS(superclass))) {
        return bagel_error("Superclass must be a class.");
    }
    ObjClass* klass = newClass(cts(name));
    pushTemp(OBJ_VAL(klass));
    if (IS_CLASS(superclass)) {
        pushTemp(superclass);
        tableAddAll(&AS_CLASS(superclass)->methods, &klass->methods);
        klass->init = AS_CLASS(superclass)->init;
        klass->superclass = AS_CLASS(superclass);
        popTemp(1);
    }
    for (const BagelMethod* method = methods; method->name != NULL; method++) {
        ObjString* methodname = cts(method->name);
        pushTemp(OBJ_VAL(methodname));
        BagelValue func = OBJ_VAL(newNative(method->method, method->arity));
        pushTemp(func);
        tableSet(&klass->methods, methodname, func);
        if (methodname == vm.initString) {
            klass->init = func;
        }
        popTemp(2);
    }
    popTemp(1); // klass
    return OBJ_VAL(klass);
}

bool bagel_hasfield(BagelValue instance, const char* name) {
    if (!IS_INSTANCE(instance)) {
        // TODO: api check
        return false;
    }
    ObjInstance* inst = AS_INSTANCE(instance);
    ObjString* string = cts(name);
    BagelValue value;
    return (tableGet(&inst->fields, string, &value)
            || tableGet(&inst->klass->methods, string, &value));
}

BagelValue bagel_getfield(BagelValue instance, const char* name) {
    if (!IS_INSTANCE(instance)) {
        // TODO: api check
        return bagel_error("Only instances have properties.");
    }
    ObjInstance* inst = AS_INSTANCE(instance);
    ObjString* string = cts(name);
    BagelValue value;
    if (!tableGet(&inst->fields, string, &value)) {
        pushTemp(instance);
        pushTemp(OBJ_VAL(string));
        bindMethod(inst->klass, string, &value);
        popTemp(2);
    }
    // bindMethod produces either the bound method or the exception
    return value;
}

void bagel_setfield(BagelValue instance, const char* name,
                    BagelValue newvalue) {
    if (!IS_INSTANCE(instance)) {
        // TODO: api check
        return;
    }
    pushTemp(instance);
    ObjInstance* inst = AS_INSTANCE(instance);
    ObjString* string = cts(name);
    pushTemp(OBJ_VAL(string));
    tableSet(&inst->fields, string, newvalue);
    popTemp(2);
}

bool bagel_deletefield(BagelValue instance, const char* name) {
    if (!IS_INSTANCE(instance)) {
        // TODO: api check
        return false;
    }
    ObjInstance* inst = AS_INSTANCE(instance);
    ObjString* string = cts(name);
    return tableDelete(&inst->fields, string);
}

BagelValue bagel_this(void) {
    BagelValue self = currentFrame.slots[0];
    if (!IS_INSTANCE(self)) {
        // TODO: api check
        return BAGEL_NIL;
    }
    return self;
}

BagelValue bagel_super(const char* field) {
    BagelValue self = bagel_this();
    if (!IS_INSTANCE(self)) {
        return BAGEL_NIL;
    }
    ObjClass* superclass = AS_INSTANCE(self)->klass->superclass;
    if (superclass == NULL) {
        return BAGEL_NIL;
    } else {
        return OBJ_VAL(superclass);
    }
}

static int getArity(BagelValue func) {
    if (IS_CLOSURE(func)) {
        return AS_CLOSURE(func)->function->arity;
    } else if (IS_NATIVE(func)) {
        return AS_NATIVE(func)->arity;
    } else if (IS_CLASS(func)) {
        BagelValue init = AS_CLASS(func)->init;
        if (IS_NIL(init)) {
            return 0;
        } else {
            return getArity(init);
        }
    } else {
        return -1;
    }
}

static BagelValue docall(BagelValue func, BagelValue zero, int nargs,
                         BagelValue* args) {
    if (IS_BOUND_METHOD(func)) {
        ObjBoundMethod* bound = AS_BOUND_METHOD(func);
        func = bound->method;
        zero = bound->receiver;
    }
    int arity = getArity(func);
    if (arity == -1) {
        return bagel_error("Can only call functions and classes.");
    }
    if (nargs != arity) {
        return argerror(arity, nargs);
    }
    if (vm.frameCount == FRAMES_MAX) {
        return bagel_error("Stack overflow.");
    }
    BagelValue* top = vm.stackTop;
    CallFrame* frame = &vm.frames[vm.frameCount++];
    frame->slots = vm.stackTop;
    bagel_push(zero);
    for (int arg = 0; arg < nargs; arg++) {
        bagel_push(args[arg]);
    }
call2:
    if (IS_CLOSURE(func)) {
        frame->closure = AS_CLOSURE(func);
        frame->ip = frame->closure->function->chunk.code;
        BagelValue result = run();
        vm.frameCount--;
        vm.stackTop = top;
        return result;
    } else if (IS_NATIVE(func)) {
        frame->closure = NULL;
        frame->ip = NULL;
        BagelValue result = AS_NATIVE(func)->function(nargs, frame->slots + 1);
        vm.frameCount--;
        return result;
    } else if (IS_CLASS(func)) {
        ObjClass* klass = AS_CLASS(func);
        *top = OBJ_VAL(newInstance(klass));
        if (!IS_NIL(klass->init)) {
            func = klass->init;
            goto call2;
        } else {
            assert(vm.stackTop == top + 1);
            return bagel_pop(1);
        }
    } else {
        assert(false); // all other cases should be handled by getArity()
    }
}

BagelValue bagel_call(BagelValue func, int nargs, BagelValue* args) {
    return docall(func, func, nargs, args);
}

BagelValue bagel_invoke(BagelValue instance, const char* method, int nargs,
                        BagelValue* args) {
    // TODO: Check memory safety (looks safe?)
    if (!IS_INSTANCE(instance)) {
        return bagel_error("Only instances have methods.");
    }
    ObjInstance* inst = AS_INSTANCE(instance);
    ObjString* string = cts(method);
    BagelValue value;
    if (tableGet(&inst->fields, string, &value)) {
        return docall(value, value, nargs, args);
    } else if (tableGet(&inst->klass->methods, string, &value)) {
        return docall(value, instance, nargs, args);
    } else {
        return bagel_error("Undefined property '%s'.", method);
    }
}

BagelValue bagel_superinvoke(const char* method, int nargs, BagelValue* args) {
    // TODO: Check memory safety (looks safe?)
    BagelValue self = bagel_this();
    if (IS_NIL(self)) {
        return bagel_error("Cannot invoke super outside a class.");
    }
    ObjClass* superclass = AS_INSTANCE(self)->klass->superclass;
    if (superclass == NULL) {
        return bagel_error("Class has no superclass.");
    }
    ObjString* string = cts(method);
    BagelValue value;
    if (tableGet(&superclass->methods, string, &value)) {
        return docall(value, self, nargs, args);
    } else {
        return bagel_error("Undefined property '%s'.", method);
    }
}

BagelValue bagel_getglobal(const char* name) {
    ObjString* string = cts(name);
    BagelValue value;
    if (tableGet(&vm.globals, string, &value)) {
        return value;
    } else {
        return bagel_error("Undefined variable '%s'.", name);
    }
}

void bagel_setglobal(const char* name, BagelValue newvalue) {
    ObjString* string = cts(name);
    pushTemp(OBJ_VAL(string));
    tableSet(&vm.globals, string, newvalue);
    popTemp(1);
}

const char* typenames[] = {
    [BAGEL_TNIL] = "nil",
    [BAGEL_TBOOL] = "boolean",
    [BAGEL_TNUMBER] = "number",
    [BAGEL_TERROR] = "error",
    [BAGEL_TSTRING] = "string",
    [BAGEL_TCLASS] = "class",
    [BAGEL_TINSTANCE] = "instance",
    [BAGEL_TFUNC] = "function",
    [BAGEL_TMETHOD] = "bound method",
    [BAGEL_TDATA] = "data",
};

BagelType bagel_type(BagelValue value) {
    switch (value.type) {
        case VAL_NIL:
            return BAGEL_TNIL;
        case VAL_BOOL:
            return BAGEL_TBOOL;
        case VAL_NUMBER:
            return BAGEL_TNUMBER;
        case VAL_ERROR:
            return BAGEL_TERROR;
        case VAL_OBJ:
            switch (OBJ_TYPE(value)) {
                case OBJ_STRING:
                    return BAGEL_TSTRING;
                case OBJ_CLASS:
                    return BAGEL_TCLASS;
                case OBJ_INSTANCE:
                    return BAGEL_TINSTANCE;
                case OBJ_CLOSURE:
                case OBJ_NATIVE:
                    return BAGEL_TFUNC;
                case OBJ_BOUND_METHOD:
                    return BAGEL_TMETHOD;
                case OBJ_DATA:
                    return BAGEL_TDATA;
                case OBJ_UPVALUE:
                case OBJ_FUNCTION:
                    assert(false); // unreachable
            }
    }
    assert(false);
}

bool bagel_tobool(BagelValue value) {
    return !isFalsey(value);
}

double bagel_todoublex(BagelValue floatvalue, bool* success) {
    if (IS_NUMBER(floatvalue)) {
        if (success != NULL) {
            *success = true;
        }
        return AS_NUMBER(floatvalue);
    } else {
        if (success != NULL) {
            *success = false;
        }
        return 0.0;
    }
}

int64_t bagel_tointx(BagelValue value, bool* success) {
    if (!IS_NUMBER(value)) {
        if (success != NULL) {
            *success = false;
        }
        return 0;
    }
    double n = AS_NUMBER(value);
    bool good = n >= (double)INT64_MIN && n < -(double)INT64_MIN;
    if (success != NULL) {
        *success = good;
    }
    if (good) {
        return (int64_t)n;
    } else {
        return 0;
    }
}

const char* bagel_tostringn(BagelValue stringvalue, size_t* length) {
    if (IS_STRING(stringvalue)) {
        if (length != NULL) {
            *length = AS_STRING(stringvalue)->length;
        }
        return AS_CSTRING(stringvalue);
    } else {
        return NULL;
    }
}

void* bagel_todata(BagelValue value) {
    if (!IS_DATA(value)) {
        return NULL;
    }
    return AS_DATA(value)->data;
}

const char* bagel_datatype(BagelValue value) {
    if (!IS_DATA(value)) {
        return NULL;
    }
    return AS_DATA(value)->type;
}

void* bagel_checkdata(BagelValue value, const char* type) {
    if (!IS_DATA(value) || strcmp(type, AS_DATA(value)->type)) {
        return NULL;
    }
    return AS_DATA(value)->data;
}

BagelValue bagel_argerror(int arg, BagelType expected) {
    if (vm.stackTop <= currentFrame.slots) {
        // TODO: api check
        return bagel_error("Bad argument #%d: expected %s, got unknown", arg,
                           typenames[expected]);
    }
    BagelType actual = bagel_type(currentFrame.slots[arg]);
    return bagel_error("Bad argument #%d: expected %s, got %s", arg,
                       typenames[expected], typenames[actual]);
}

BagelValue bagel_bool(bool value) {
    return value ? BAGEL_TRUE : BAGEL_FALSE;
}

BagelValue bagel_int(int64_t value) {
    return bagel_double((double)value);
}

BagelValue bagel_double(double value) {
    return NUMBER_VAL(value);
}

BagelValue bagel_string(const char* cstring) {
    return OBJ_VAL(cts(cstring));
}

BagelValue bagel_stringn(const char* cstring, size_t length) {
    return OBJ_VAL(copyString(cstring, length));
}

BagelValue bagel_data(size_t size, const char* type) {
    return OBJ_VAL(newData(size, type));
}

BagelValue bagel_cfunc(BagelCFunc func, int arity) {
    return OBJ_VAL(newNative(func, arity));
}

void bagel_setcfunc(const char* name, BagelCFunc func, int arity) {
    BagelValue value = bagel_cfunc(func, arity);
    pushTemp(value);
    bagel_setglobal(name, value);
    popTemp(1);
}

#define BINARY_OP(type, op) \
    do { \
        if (!IS_NUMBER(left) || !IS_NUMBER(right)) { \
            return BAGEL_NIL; \
        } \
        return type(AS_NUMBER(left) op AS_NUMBER(right)); \
    } while (false)

BagelValue bagel_op(BagelValue left, BagelValue right, BagelOp op) {
    switch (op) {
        case BAGEL_OPEQ: {
            return BOOL_VAL(valuesEqual(left, right));
        }
        case BAGEL_OPNE: {
            return BOOL_VAL(!valuesEqual(left, right));
        }
        case BAGEL_OPLT: {
            BINARY_OP(BOOL_VAL, <);
        }
        case BAGEL_OPLE: {
            BINARY_OP(BOOL_VAL, <=);
        }
        case BAGEL_OPGT: {
            BINARY_OP(BOOL_VAL, >);
        }
        case BAGEL_OPGE: {
            BINARY_OP(BOOL_VAL, >=);
        }
        case BAGEL_OPADD: {
            if (IS_STRING(left) && IS_STRING(right)) {
                ObjString* a = AS_STRING(left);
                ObjString* b = AS_STRING(right);

                int length = a->length + b->length;
                pushTemp(left);
                pushTemp(right);
                char* chars = ALLOCATE(char, length + 1);
                memcpy(chars, a->chars, a->length);
                memcpy(chars + a->length, b->chars, b->length);
                chars[length] = '\0';
                popTemp(2);

                return OBJ_VAL(takeString(chars, length));
            } else if (IS_NUMBER(left) && IS_NUMBER(right)) {
                return NUMBER_VAL(AS_NUMBER(left) + AS_NUMBER(right));
            } else {
                return BAGEL_NIL;
            }
        }
        case BAGEL_OPSUB: {
            BINARY_OP(NUMBER_VAL, -);
        }
        case BAGEL_OPMUL: {
            BINARY_OP(NUMBER_VAL, *);
        }
        case BAGEL_OPDIV: {
            BINARY_OP(NUMBER_VAL, /);
        }
        case BAGEL_OPMOD: {
            if (!IS_NUMBER(left) || !IS_NUMBER(right)) {
                return BAGEL_NIL;
            }
            double a = AS_NUMBER(left);
            double b = AS_NUMBER(right);
            if ((i64)a != a || (i64)b != b) {
                return BAGEL_NIL;
            }
            return NUMBER_VAL((double)((i64)a % (i64)b));
        }
        case BAGEL_OPPOW: {
            if (!IS_NUMBER(left) || !IS_NUMBER(right)) {
                return BAGEL_NIL;
            }
            return NUMBER_VAL(pow(AS_NUMBER(left), AS_NUMBER(right)));
        }
        case BAGEL_OPNEG: {
            if (!IS_NUMBER(left)) {
                return BAGEL_NIL;
            }
            return NUMBER_VAL(-AS_NUMBER(left));
        }
        default: {
            assert(false); // unreachable
            return BAGEL_NIL;
        }
    }
}

void bagel_push(BagelValue value) {
    if (vm.stackTop >= vm.stack + STACK_MAX) {
        // TODO: dynamic stack
        return;
    }
    *vm.stackTop = value;
    vm.stackTop++;
}

BagelValue bagel_pop(int count) {
    if (vm.stackTop - count < currentFrame.slots) {
        // TODO: api check
        return bagel_error("api error");
    }
    vm.stackTop -= count;
    return *vm.stackTop;
}
