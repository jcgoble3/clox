#ifndef bagel_common_h
#define bagel_common_h

// This include must be first to ensure that bagel.h contains its own
// C standard library includes, because external libraries may be compiled
// with only bagel.h present
#include "bagel.h"

#include <assert.h>
#include <math.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

//#define DEBUG_PRINT_CODE
//#define DEBUG_TRACE_EXECUTION

//#define DEBUG_STRESS_GC
//#define DEBUG_LOG_GC

#define UINT8_COUNT (UINT8_MAX + 1)

// I like Rust-style typenames better
typedef int64_t i64;
typedef uint32_t u32;
typedef int32_t i32;
typedef int16_t i16;
typedef uint8_t u8;
typedef int8_t i8;

// Moving toward enabling unused parameter/function warnings
// https://stackoverflow.com/questions/3599160/how-to-suppress-unused-parameter-warnings-in-c
// Note that the addition of "UNUSED_" ensures that these don't stick around
// being put into use

#ifdef __GNUC__
#define UNUSED(x) UNUSED_##x __attribute__((__unused__))
#else
#define UNUSED(x) UNUSED_##x
#endif

#ifdef __GNUC__
#define UNUSED_FUNCTION(x) __attribute__((__unused__)) UNUSED_##x
#else
#define UNUSED_FUNCTION(x) UNUSED_##x
#endif

// Include everything - ORDER MATTERS
// Each header must be below all of its dependencies
// Inter-header dependencies are noted
#include "value.h" // common
#include "table.h" // value, common
#include "chunk.h" // value, table, common
#include "object.h" // value, table, chunk, common
#include "vm.h" // value, table, chunk, object
#include "compiler.h" // object, vm
#include "debug.h" // chunk
#include "memory.h" // object, common
#include "scanner.h" // (none)

#endif // include guard
