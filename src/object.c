#include "common.h"

#define ALLOCATE_OBJ(type, objectType) \
    (type*)allocateObject(sizeof(type), objectType)

static Obj* allocateObject(size_t size, ObjType type) {
    Obj* object = (Obj*)reallocate(NULL, 0, size);
    object->type = type;
    object->isMarked = false;

    object->next = vm.objects;
    vm.objects = object;

#ifdef DEBUG_LOG_GC
    printf("%p allocate %zu for %d\n", (void*)object, size, type);
#endif

    return object;
}

ObjBoundMethod* newBoundMethod(BagelValue receiver, BagelValue method) {
    ObjBoundMethod* bound = ALLOCATE_OBJ(ObjBoundMethod, OBJ_BOUND_METHOD);
    bound->receiver = receiver;
    bound->method = method;
    return bound;
}

ObjClass* newClass(ObjString* name) {
    ObjClass* klass = ALLOCATE_OBJ(ObjClass, OBJ_CLASS);
    klass->name = name;
    initMethodArray(&klass->rawMethods);
    initTable(&klass->methods);
    klass->init = NIL_VAL;
    klass->superclass = NULL;
    return klass;
}

ObjClosure* newClosure(ObjFunction* function) {
    ObjUpvalue** upvalues = ALLOCATE(ObjUpvalue*, function->upvalueCount);
    for (int i = 0; i < function->upvalueCount; i++) {
        upvalues[i] = NULL;
    }
    ObjClosure* closure = ALLOCATE_OBJ(ObjClosure, OBJ_CLOSURE);
    closure->function = function;
    closure->upvalues = upvalues;
    closure->upvalueCount = function->upvalueCount;
    return closure;
}

ObjData* newData(size_t size, const char* type) {
    void* ptr = ALLOCATE(char, size);
    char* buffer = ALLOCATE(char, strlen(type) + 1);
    strcpy(buffer, type);
    ObjData* dataobj = ALLOCATE_OBJ(ObjData, OBJ_DATA);
    dataobj->type = buffer;
    dataobj->data = ptr;
    dataobj->size = size;
    return dataobj;
}

ObjFunction* newFunction() {
    ObjFunction* function = ALLOCATE_OBJ(ObjFunction, OBJ_FUNCTION);

    function->arity = 0;
    function->upvalueInfo = NULL;
    function->upvalueCount = 0;
    function->name = NULL;
    initChunk(&function->chunk);
    return function;
}

ObjInstance* newInstance(ObjClass* klass) {
    ObjInstance* instance = ALLOCATE_OBJ(ObjInstance, OBJ_INSTANCE);
    instance->klass = klass;
    initTable(&instance->fields);
    return instance;
}

ObjNative* newNative(BagelCFunc function, int arity) {
    ObjNative* native = ALLOCATE_OBJ(ObjNative, OBJ_NATIVE);
    native->arity = arity;
    native->function = function;
    return native;
}

static ObjString* allocateString(char* chars, int length, uint32_t hash) {
    ObjString* string = ALLOCATE_OBJ(ObjString, OBJ_STRING);
    string->length = length;
    string->chars = chars;
    string->hash = hash;

    pushTemp(OBJ_VAL(string));
    tableSet(&vm.strings, string, NIL_VAL);
    popTemp(1);

    return string;
}

// FNV-1a: http://www.isthe.com/chongo/tech/comp/fnv/
static uint32_t hashString(const char* key, int length) {
    uint32_t hash = 2166136261u;

    for (int i = 0; i < length; i++) {
        hash ^= (uint8_t)key[i];
        hash *= 16777619;
    }

    return hash;
}

ObjString* takeString(char* chars, int length) {
    uint32_t hash = hashString(chars, length);
    ObjString* interned = tableFindString(&vm.strings, chars, length, hash);
    if (interned != NULL) {
        FREE_ARRAY(char, chars, length + 1);
        return interned;
    }
    return allocateString(chars, length, hash);
}

ObjString* copyString(const char* chars, int length) {
    uint32_t hash = hashString(chars, length);
    ObjString* interned = tableFindString(&vm.strings, chars, length, hash);
    if (interned != NULL) {
        return interned;
    }

    char* heapChars = ALLOCATE(char, length + 1);
    memcpy(heapChars, chars, length);
    heapChars[length] = '\0';

    return allocateString(heapChars, length, hash);
}

ObjUpvalue* newUpvalue(BagelValue* slot) {
    ObjUpvalue* upvalue = ALLOCATE_OBJ(ObjUpvalue, OBJ_UPVALUE);
    upvalue->closed = NIL_VAL;
    upvalue->location = slot;
    return upvalue;
}

static int functionToString(char* buf, ObjFunction* function) {
    if (function->name == NULL) {
        return snprintf(buf, 97, "<script>");
    } else {
        return snprintf(buf, 97, "<fn %s>", function->name->chars);
    }
}

static void printFunction(ObjFunction* function) {
    if (function->name == NULL) {
        printf("<script>");
        return;
    }
    printf("<fn %s>", function->name->chars);
}

ObjString* objectToString(BagelValue value) {
    char buf[100];
    int count;
    switch (OBJ_TYPE(value)) {
        case OBJ_BOUND_METHOD:
            return objectToString(AS_BOUND_METHOD(value)->method);
        case OBJ_CLASS:
            count
                = snprintf(buf, 97, "class %s", AS_CLASS(value)->name->chars);
            break;
        case OBJ_CLOSURE:
            count = functionToString(buf, AS_CLOSURE(value)->function);
            break;
        case OBJ_FUNCTION:
            count = functionToString(buf, AS_FUNCTION(value));
            break;
        case OBJ_INSTANCE:
            count = snprintf(buf, 97, "%s instance",
                             AS_INSTANCE(value)->klass->name->chars);
            break;
        case OBJ_NATIVE:
            return copyString("<native fn>", 11);
        case OBJ_STRING:
            return AS_STRING(value);
        default: // OBJ_UPVALUE
            return NULL; // Unreachable
    }
    if (count > 97) {
        buf[97] = '.';
        buf[98] = '.';
        buf[99] = '.';
        count = 100;
    }
    return copyString(buf, count);
}

void printObject(BagelValue value) {
    switch (OBJ_TYPE(value)) {
        case OBJ_BOUND_METHOD:
            printObject(AS_BOUND_METHOD(value)->method);
            break;
        case OBJ_CLASS:
            printf("class %s", AS_CLASS(value)->name->chars);
            break;
        case OBJ_CLOSURE:
            printFunction(AS_CLOSURE(value)->function);
            break;
        case OBJ_DATA:
            printf("data at %p", AS_DATA(value)->data);
            break;
        case OBJ_FUNCTION:
            printFunction(AS_FUNCTION(value));
            break;
        case OBJ_INSTANCE:
            printf("%s instance", AS_INSTANCE(value)->klass->name->chars);
            break;
        case OBJ_NATIVE:
            printf("<native fn>");
            break;
        case OBJ_STRING:
            printf("%s", AS_CSTRING(value));
            break;
        case OBJ_UPVALUE:
            // This is actually unreachable
            printf("upvalue");
            break;
    }
}

void initMethodArray(MethodArray* array) {
    array->funcs = NULL;
    array->names = NULL;
    array->capacity = 0;
    array->count = 0;
}

void writeMethodArray(MethodArray* array, BagelValue name, BagelValue func) {
    if (array->capacity < array->count + 1) {
        int oldCapacity = array->capacity;
        array->capacity = GROW_CAPACITY(oldCapacity);
        array->names = GROW_ARRAY(BagelValue, array->names, oldCapacity,
                                  array->capacity);
        array->funcs = GROW_ARRAY(BagelValue, array->funcs, oldCapacity,
                                  array->capacity);
    }

    array->names[array->count] = name;
    array->funcs[array->count] = func;
    array->count++;
}

void freeMethodArray(MethodArray* array) {
    FREE_ARRAY(BagelValue, array->funcs, array->capacity);
    FREE_ARRAY(BagelValue, array->names, array->capacity);
    initMethodArray(array);
}
