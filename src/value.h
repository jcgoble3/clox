#ifndef bagel_value_h
#define bagel_value_h

typedef struct Obj Obj;
typedef struct ObjString ObjString;

// This can be changed back to an enum when BagelValue details are
// no longer public
#define VAL_ERROR  0
#define VAL_BOOL   1
#define VAL_NIL    2
#define VAL_NUMBER 3
#define VAL_OBJ    4

#define IS_BOOL(value)   ((value).type == VAL_BOOL)
#define IS_NIL(value)    ((value).type == VAL_NIL)
#define IS_NUMBER(value) ((value).type == VAL_NUMBER)
#define IS_OBJ(value)    ((value).type == VAL_OBJ)

#define AS_OBJ(value)    ((Obj*)(value).as.obj)
#define AS_BOOL(value)   ((value).as.boolean)
#define AS_NUMBER(value) ((value).as.number)

#define ERROR_VAL         ((BagelValue){VAL_ERROR, {.number = 0}})
#define BOOL_VAL(value)   ((BagelValue){VAL_BOOL, {.boolean = value}})
#define NIL_VAL           ((BagelValue){VAL_NIL, {.number = 0}})
#define NUMBER_VAL(value) ((BagelValue){VAL_NUMBER, {.number = value}})
#define OBJ_VAL(object)   ((BagelValue){VAL_OBJ, {.obj = (Obj*)object}})

typedef struct {
    int capacity;
    int count;
    BagelValue* values;
} ValueArray;

bool valuesEqual(BagelValue a, BagelValue b);
void initValueArray(ValueArray* array);
void writeValueArray(ValueArray* array, BagelValue value);
void freeValueArray(ValueArray* array);
ObjString* toString(BagelValue value);
void printValue(BagelValue value);

#endif // include guard
