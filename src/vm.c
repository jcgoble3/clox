#include "common.h"

// Assuming we are in the scope of a variable named "frame"...
#define IP    (frame->ip)
#define CHUNK (frame->closure->function->chunk)

VM vm;

static void resetStack() {
    vm.tempTop = vm.tempRoot;
    vm.stackTop = vm.stack;
    vm.frameCount = 0;
    vm.openUpvalues = NULL;
}

void runtimeError(const char* format, va_list args) {
    vfprintf(stderr, format, args);
    fputs("\n", stderr);

    for (int i = vm.frameCount - 1; i >= 0; i--) {
        CallFrame* frame = &vm.frames[i];
        if (frame->closure == NULL) {
            // Native function
            // TODO: function name
            fprintf(stderr, "[C call]\n");
        } else {
            ObjFunction* function = frame->closure->function;
            // -1 because the IP is sitting on the next instruction
            size_t instruction = IP - function->chunk.code - 1;
            int line = getLine(&function->chunk, instruction);
            fprintf(stderr, "[line %d] in ", line);
            if (function->name == NULL) {
                fprintf(stderr, "<script>\n");
            } else {
                fprintf(stderr, "%s()\n", function->name->chars);
            }
        }
    }

    resetStack();
}

void initVM() {
    resetStack();
    vm.lastStackTop = vm.stack;
    vm.objects = NULL;
    vm.bytesAllocated = 0;
    vm.nextGC = 1024 * 1024;
    vm.grayCount = 0;
    vm.grayCapacity = 0;
    vm.grayStack = NULL;
    initTable(&vm.globals);
    initTable(&vm.strings);

    vm.initString = NULL; // ensure the GC never sees uninitialized memory
    vm.initString = copyString("init", 4);

    bagel_loadstdlib();
}

void freeVM() {
    freeTable(&vm.globals);
    freeTable(&vm.strings);
    vm.initString = NULL;
    freeObjects();
}

void pushTemp(BagelValue value) {
    *vm.tempTop = value;
    vm.tempTop++;
}

void popTemp(int n) {
    vm.tempTop -= n;
}

BagelValue argerror(int arity, int actual) {
    return bagel_error("Expected %d argument%s but got %d.", arity,
                       arity == 1 ? "" : "s", actual);
}

static bool call(ObjClosure* closure, BagelValue* frameStart, int argCount) {
    if (argCount != closure->function->arity) {
        *frameStart = argerror(closure->function->arity, argCount);
        return false;
    }
    if (vm.frameCount == FRAMES_MAX) {
        *frameStart = bagel_error("Stack overflow.");
        return false;
    }
    CallFrame* frame = &vm.frames[vm.frameCount++];
    frame->closure = closure;
    IP = closure->function->chunk.code;

    frame->slots = frameStart;
    return true;
}

static bool callNative(ObjNative* native, BagelValue* frameStart,
                       int argCount) {
    if (argCount != native->arity) {
        *frameStart = argerror(native->arity, argCount);
        return false;
    }
    if (vm.frameCount == FRAMES_MAX) {
        *frameStart = bagel_error("Stack overflow.");
        return false;
    }
    CallFrame* frame = &vm.frames[vm.frameCount++];
    frame->closure = NULL;
    IP = NULL;
    frame->slots = frameStart;
    vm.stackTop = frameStart + argCount + 1;
    BagelValue result = native->function(argCount, frameStart + 1);
    vm.frameCount--;
    *frameStart = result;
    return (result.type != VAL_ERROR);
}

static bool callAny(BagelValue func, BagelValue* frameStart, int argCount) {
    if (IS_CLOSURE(func)) {
        return call(AS_CLOSURE(func), frameStart, argCount);
    } else if (IS_NATIVE(func)) {
        return callNative(AS_NATIVE(func), frameStart, argCount);
    } else {
        assert(false);
    }
}

static bool callValue(BagelValue* callee, int argCount) {
    if (IS_OBJ(*callee)) {
        switch (OBJ_TYPE(*callee)) {
            case OBJ_BOUND_METHOD: {
                ObjBoundMethod* bound = AS_BOUND_METHOD(*callee);
                *callee = bound->receiver;
                return callAny(bound->method, callee, argCount);
            }
            case OBJ_CLASS: {
                ObjClass* klass = AS_CLASS(*callee);
                vm.stackTop = callee + argCount + 1;
                *callee = OBJ_VAL(newInstance(klass));
                if (!IS_NIL(klass->init)) {
                    return callAny(klass->init, callee, argCount);
                } else if (argCount != 0) {
                    *callee = argerror(0, argCount);
                    return false;
                }
                return true;
            }
            case OBJ_CLOSURE:
            case OBJ_NATIVE:
                return callAny(*callee, callee, argCount);
            default:
                // Non-callable object type
                break;
        }
    }
    *callee = bagel_error("Can only call functions and classes.");
    return false;
}

static bool invokeFromClass(ObjClass* klass, BagelValue* frameStart,
                            ObjString* name, int argCount) {
    BagelValue method;
    if (!tableGet(&klass->methods, name, &method)) {
        *frameStart = bagel_error("Undefined property '%s'.", name->chars);
        return false;
    }
    return callAny(method, frameStart, argCount);
}

static bool invoke(BagelValue* receiver, ObjString* name, int argCount) {
    if (!IS_INSTANCE(*receiver)) {
        *receiver = bagel_error("Only instances have methods.");
        return false;
    }
    ObjInstance* instance = AS_INSTANCE(*receiver);
    BagelValue value;
    if (tableGet(&instance->fields, name, &value)) {
        *receiver = value;
        return callValue(receiver, argCount);
    }
    return invokeFromClass(instance->klass, receiver, name, argCount);
}

bool bindMethod(ObjClass* klass, ObjString* name, BagelValue* dest) {
    BagelValue method;
    if (!tableGet(&klass->methods, name, &method)) {
        *dest = bagel_error("Undefined property '%s'.", name->chars);
        return false;
    }

    ObjBoundMethod* bound = newBoundMethod(*dest, method);
    *dest = OBJ_VAL(bound);
    return true;
}

static ObjUpvalue* captureUpvalue(BagelValue* local) {
    ObjUpvalue* prevUpvalue = NULL;
    ObjUpvalue* upvalue = vm.openUpvalues;

    while (upvalue != NULL && upvalue->location > local) {
        prevUpvalue = upvalue;
        upvalue = upvalue->next;
    }
    if (upvalue != NULL && upvalue->location == local) {
        return upvalue;
    }

    ObjUpvalue* createdUpvalue = newUpvalue(local);
    createdUpvalue->next = upvalue;

    if (prevUpvalue == NULL) {
        vm.openUpvalues = createdUpvalue;
    } else {
        prevUpvalue->next = createdUpvalue;
    }
    return createdUpvalue;
}

static void closeUpvalues(BagelValue* last) {
    while (vm.openUpvalues != NULL && vm.openUpvalues->location >= last) {
        ObjUpvalue* upvalue = vm.openUpvalues;
        upvalue->closed = *upvalue->location;
        upvalue->location = &upvalue->closed;
        vm.openUpvalues = upvalue->next;
    }
}

bool isFalsey(BagelValue value) {
    return IS_NIL(value) || (IS_BOOL(value) && !AS_BOOL(value));
}

BagelValue run(void) {
    CallFrame* frame = &vm.frames[vm.frameCount - 1];

#define R(r)       (frame->slots[r])
#define RB(i)      (R(B(i)))
#define RC(i)      (R(C(i)))
#define K(k)       (CHUNK.constants.values[k])
#define RKB(i)     (flag(i) ? K(B(i)) : RB(i))
#define UPVALUE(u) (frame->closure->upvalues[u]->location)
#define eBx(i) \
    (flag(i) ? (i2 = *IP++, assert(op(i2) == OPR_EXTRAARG), Ax(i2)) : Bx(i))
#define BINARY_OP(type, op) \
    do { \
        if (!IS_NUMBER(RB(i)) || !IS_NUMBER(RC(i))) { \
            return bagel_error("Operands must be numbers."); \
        } \
        *ra = type(AS_NUMBER(RB(i)) op AS_NUMBER(RC(i))); \
    } while (false)
#define FRAME_TOP     (frame->slots + UINT8_COUNT)
#define PROTECT_TOP() vm.stackTop = FRAME_TOP

    for (;;) {
#ifdef DEBUG_TRACE_EXECUTION
        printf("          ");
        vm.stackTop = frame->slots + 10;
        for (BagelValue* slot = vm.stack; slot < vm.stackTop; slot++) {
            printf("[ ");
            printValue(*slot);
            printf(" ]");
        }
        printf("\n");
        int offset = (int)(IP - CHUNK.code);
        disassembleInstruction(&CHUNK, offset, getLine(&CHUNK, offset));
#endif

        Instruction i, i2;
        BagelValue* ra;
        assert(((void)(vm.stackTop = frame->slots), 1));
        switch (i = *IP++, ra = &R(A(i)), op(i)) {
            case OPR_MOVE: {
                *ra = RB(i);
                break;
            }

            case OPR_LOADK: {
                *ra = K(eBx(i));
                break;
            }

            case OPR_LOADI: {
                *ra = NUMBER_VAL((double)sBx(i));
                break;
            }

            case OPR_LOADNIL: {
                *ra = NIL_VAL;
                break;
            }

            case OPR_LOADBOOL: {
                *ra = BOOL_VAL((bool)B(i));
                break;
            }

            case OPR_GETGLOBAL: {
                ObjString* name = AS_STRING(K(eBx(i)));
                BagelValue value;
                PROTECT_TOP();
                if (!tableGet(&vm.globals, name, &value)) {
                    return bagel_error("Undefined variable '%s'.",
                                       name->chars);
                }
                *ra = value;
                break;
            }

            case OPR_SETGLOBAL: {
                ObjString* name = AS_STRING(K(eBx(i)));
                PROTECT_TOP();
                if (tableSet(&vm.globals, name, *ra)) {
                    tableDelete(&vm.globals, name);
                    return bagel_error("Undefined variable '%s'.",
                                       name->chars);
                }
                break;
            }

            case OPR_DEFGLOBAL: {
                ObjString* name = AS_STRING(K(eBx(i)));
                PROTECT_TOP();
                tableSet(&vm.globals, name, *ra);
                break;
            }

            case OPR_GETUPVAL: {
                *ra = *UPVALUE(B(i));
                break;
            }

            case OPR_SETUPVAL: {
                *UPVALUE(B(i)) = *ra;
                break;
            }

            case OPR_GETATTR: {
                if (!IS_INSTANCE(*ra)) {
                    return bagel_error("Only instances have properties.");
                }
                ObjInstance* instance = AS_INSTANCE(*ra);
                ObjString* name = AS_STRING(K(eBx(i)));
                BagelValue value;
                PROTECT_TOP();
                if (tableGet(&instance->fields, name, &value)) {
                    *ra = value;
                    break;
                }
                if (!bindMethod(instance->klass, name, ra)) {
                    return *ra; // error object
                }
                break;
            }

            case OPR_SETATTR: {
                if (!IS_INSTANCE(*ra)) {
                    return bagel_error("Only instances have fields.");
                }
                ObjInstance* instance = AS_INSTANCE(*ra);
                ObjString* name = AS_STRING(RKB(i));
                PROTECT_TOP();
                tableSet(&instance->fields, name, RC(i));
                *ra = RC(i);
                break;
            }

            case OPR_GETSUPER: {
                ObjClass* superclass = AS_CLASS(*UPVALUE(C(i)));
                ObjString* name = AS_STRING(RKB(i));
                PROTECT_TOP();
                if (!bindMethod(superclass, name, ra)) {
                    return *ra; // error object
                }
                break;
            }

            case OPR_CASE: {
                i32 jump = eBx(i);
                if (!valuesEqual(*ra, R(A(i) + 1))) {
                    IP += jump;
                }
                break;
            }

            case OPR_EQ: {
                *ra = BOOL_VAL(valuesEqual(RB(i), RC(i)));
                break;
            }

            case OPR_NE: {
                *ra = BOOL_VAL(!valuesEqual(RB(i), RC(i)));
                break;
            }

            case OPR_LT: {
                BINARY_OP(BOOL_VAL, <);
                break;
            }

            case OPR_LE: {
                BINARY_OP(BOOL_VAL, <=);
                break;
            }

            case OPR_GT: {
                BINARY_OP(BOOL_VAL, >);
                break;
            }

            case OPR_GE: {
                BINARY_OP(BOOL_VAL, >=);
                break;
            }

            case OPR_ADD: {
                if (IS_STRING(RB(i)) && IS_STRING(RC(i))) {
                    ObjString* a = AS_STRING(RB(i));
                    ObjString* b = AS_STRING(RC(i));

                    int length = a->length + b->length;
                    PROTECT_TOP();
                    char* chars = ALLOCATE(char, length + 1);
                    memcpy(chars, a->chars, a->length);
                    memcpy(chars + a->length, b->chars, b->length);
                    chars[length] = '\0';

                    *ra = OBJ_VAL(takeString(chars, length));
                } else if (IS_NUMBER(RB(i)) && IS_NUMBER(RC(i))) {
                    *ra = NUMBER_VAL(AS_NUMBER(RB(i)) + AS_NUMBER(RC(i)));
                } else {
                    return bagel_error(
                        "Operands must be two numbers or two strings.");
                }
                break;
            }

            case OPR_SUB: {
                BINARY_OP(NUMBER_VAL, -);
                break;
            }

            case OPR_MUL: {
                BINARY_OP(NUMBER_VAL, *);
                break;
            }

            case OPR_DIV: {
                BINARY_OP(NUMBER_VAL, /);
                break;
            }

            case OPR_MOD: {
                if (!IS_NUMBER(RB(i)) || !IS_NUMBER(RC(i))) {
                    return bagel_error("Operands must be integers.");
                }
                double a = AS_NUMBER(RB(i));
                double b = AS_NUMBER(RC(i));
                if ((i64)a != a || (i64)b != b) {
                    return bagel_error("Operands must be integers.");
                }
                *ra = NUMBER_VAL((double)((i64)a % (i64)b));
                break;
            }

            case OPR_POW: {
                if (!IS_NUMBER(RB(i)) || !IS_NUMBER(RC(i))) {
                    return bagel_error("Operands must be numbers.");
                }
                *ra = NUMBER_VAL(pow(AS_NUMBER(RB(i)), AS_NUMBER(RC(i))));
                break;
            }

            case OPR_NEG: {
                if (!IS_NUMBER(RB(i))) {
                    return bagel_error("Operand must be a number.");
                }
                *ra = NUMBER_VAL(-AS_NUMBER(RB(i)));
                break;
            }

            case OPR_NOT: {
                *ra = BOOL_VAL(isFalsey(RB(i)));
                break;
            }

            case OPR_PRINT: {
                printValue(*ra);
                printf("\n");
                break;
            }

            case OPR_PRINTX: {
                printValue(K(Ax(i)));
                printf("\n");
                break;
            }

            case OPR_JUMP: {
                IP += sAx(i);
                break;
            }

            case OPR_JUMPONFALSE: {
                i32 jump = eBx(i);
                if (isFalsey(*ra)) {
                    IP += jump;
                }
                break;
            }

            case OPR_BUILDSTR: {
                size_t length = 0;
                vm.stackTop = ra + B(i) + 1;
                for (BagelValue* value = ra; value < ra + B(i); value++) {
                    if ((value->type != VAL_OBJ
                         || OBJ_TYPE(*value) != OBJ_STRING)) {
                        *value = OBJ_VAL(toString(*value));
                    }
                    length += AS_STRING(*value)->length;
                }
                char* newString = ALLOCATE(char, length + 1);
                char* pos = newString;
                for (BagelValue* value = ra; value < ra + B(i); value++) {
                    ObjString* str = AS_STRING(*value);
                    memcpy(pos, str->chars, str->length);
                    pos += str->length;
                }
                *pos = '\0';
                vm.stackTop = ra + 1;
                *ra = OBJ_VAL(takeString(newString, length));
                break;
            }

            case OPR_CALL: {
                if (!callValue(ra, B(i))) {
                    return *ra; // error object
                }
                frame = &vm.frames[vm.frameCount - 1];
                break;
            }

            case OPR_INVOKE: {
                if (!invoke(ra, AS_STRING(RKB(i)), C(i))) {
                    return *ra; // error object
                }
                frame = &vm.frames[vm.frameCount - 1];
                break;
            }

            case OPR_SUPERINVOKE: {
                i2 = *IP++;
                assert(op(i2) == OPR_EXTRAARG);
                if (!invokeFromClass(AS_CLASS(*UPVALUE(B(i))), ra,
                                     AS_STRING(K(Ax(i2))), C(i))) {
                    return *ra; // error object
                }
                frame = &vm.frames[vm.frameCount - 1];
                break;
            }

            case OPR_CLOSURE: {
                ObjFunction* function = AS_FUNCTION(K(eBx(i)));
                PROTECT_TOP();
                ObjClosure* closure = newClosure(function);
                *ra = OBJ_VAL(closure);
                for (int i = 0; i < closure->upvalueCount; i++) {
                    u8 index = function->upvalueInfo[i].index;
                    if (function->upvalueInfo[i].isLocal) {
                        closure->upvalues[i] = captureUpvalue(&R(index));
                    } else {
                        closure->upvalues[i] = frame->closure->upvalues[index];
                    }
                }
                break;
            }

            case OPR_CLOSEUPVAL: {
                closeUpvalues(ra);
                break;
            }

            case OPR_RETURN: {
                BagelValue result = flag(i) ? *ra : NIL_VAL;
                closeUpvalues(frame->slots);
                R(0) = result;
                if (vm.frameCount == 1) {
                    return result;
                }
                assert(vm.frameCount > 1);
                frame = &vm.frames[vm.frameCount - 2];
                if (frame->closure == NULL) {
                    // C call boundary
                    return result;
                }
                vm.frameCount--;
                break;
            }

            case OPR_CLASS: {
                ObjClass* klass = AS_CLASS(RKB(i));
                PROTECT_TOP();
                // Inheritance
                if (C(i) != NOREG) {
                    BagelValue superclass = RC(i);
                    if (!IS_CLASS(superclass)) {
                        return bagel_error("Superclass must be a class.");
                    }
                    pushTemp(OBJ_VAL(klass));
                    tableAddAll(&AS_CLASS(superclass)->methods,
                                &klass->methods);
                    klass->init = AS_CLASS(superclass)->init;
                    klass->superclass = AS_CLASS(superclass);
                    popTemp(1);
                }
                // Define methods
                for (int i = 0; i < klass->rawMethods.count; i++) {
                    ObjString* name = AS_STRING(klass->rawMethods.names[i]);
                    ObjFunction* function
                        = AS_FUNCTION(klass->rawMethods.funcs[i]);
                    ObjClosure* method = newClosure(function);
                    pushTemp(OBJ_VAL(method));
                    tableSet(&klass->methods, name, OBJ_VAL(method));
                    for (int i = 0; i < method->upvalueCount; i++) {
                        u8 index = function->upvalueInfo[i].index;
                        if (function->upvalueInfo[i].isLocal) {
                            method->upvalues[i] = captureUpvalue(&R(index));
                        } else {
                            method->upvalues[i]
                                = frame->closure->upvalues[index];
                        }
                    }
                    popTemp(1);
                    if (name == vm.initString) {
                        klass->init = OBJ_VAL(method);
                    }
                }
                closeUpvalues(&RC(i)); // close "super"
                *ra = OBJ_VAL(klass);
                break;
            }

            case OPR_EXTRAARG: {
                // Unreachable
                assert(false);
            }
        }
    }

#undef R
#undef RB
#undef RC
#undef K
#undef RKB
#undef UPVALUE
#undef eBx
#undef FRAME_TOP
#undef PROTECT_TOP

#undef BINARY_OP
}

InterpretResult interpret(const char* source) {
    ObjFunction* function = compile(source);
    if (function == NULL) {
        return INTERPRET_COMPILE_ERROR;
    }

    pushTemp(OBJ_VAL(function));
    ObjClosure* closure = newClosure(function);
    popTemp(1);
    vm.stack[0] = OBJ_VAL(closure);
    callValue(vm.stack, 0);

    BagelValue result = run();
    vm.frameCount--;
    return result.type == VAL_ERROR ? INTERPRET_RUNTIME_ERROR : INTERPRET_OK;
}
